from queue import PriorityQueue
import sys
import copy
from time import time

start = time()

bkup_q = []
bkup_board = []

def solve():
    global q
    global board

    while True:
        if q.empty():
            for i in range(n):
                for j in range(n):
                    if type(board[i][j]) != int:
                        return False
            return True

        s, i, j = q.get()
        if type(board[i][j]) == int: continue
        break

    if (len(board[i][j]) == 0): return False

    vals = board[i][j].copy()
    opts = len(vals)

    for t in vals:
        if (opts > 1):
            bkup_q.append((i, j, copy.copy(q)))
            bkup_board.append((i, j, copy.deepcopy(board)))
        board[i][j] = t
        for k in range(n):
            if type(board[i][k]) == set:
                board[i][k].discard(t)
                q.put((len(board[i][k]), i, k))
            if type(board[k][j]) == set:
                board[k][j].discard(t)
                q.put((len(board[k][j]), k, j))
        for k in range(i // m * m, i // m * m + m):
            for l in range(j // m * m, j // m * m + m):
                if type(board[k][l]) == int: continue
                board[k][l].discard(t)
                q.put((len(board[k][l]), k, l))
        if solve(): return True
        if opts > 1:
            while bkup_q[-1][0] != i or bkup_q[-1][1] != j:
                bkup_q.pop()
                bkup_board.pop()
            q = bkup_q.pop()[2]
            board = bkup_board.pop()[2]
    return False

def print_board(board):
    print()
    n = len(board)
    d = len(str(n))
    fmt = "{:>" + str(d) + "}"
    for i in range(n):
        if (i % m == 0): print(" " + "-" * ((d + 1) * n + 2 * m - 1))
        for j in range(n):
            if (j % m == 0): print("|", end=" ")
            if (board[i][j] == 0):
                print(fmt.format("."), end=" ")
            elif type(board[i][j]) == set:
                print(fmt.format("."), end=" ")
            else:
                print(fmt.format(board[i][j]), end=" ")
        print("|", end="")
        print()
    print(" " + "-" * ((d + 1) * n + 2 * m - 1))
    print()

def parseCell(val):
    if val in "0.?":
        return 0
    return int(val)

board = [list(map(parseCell, input().split()))]
n = len(board[0])
m = 1
while m ** 2 < n: m += 1
for i in range(n-1):
    board.append(list(map(parseCell, input().split())))

sys.setrecursionlimit(max(1000, 2 * n ** 2))

print_board(board)
for i in range(n):
    for j in range(n):
        if board[i][j] == 0:
            board[i][j] = set(range(1, n+1))
        else:
            board[i][j] = set([board[i][j]])

q = PriorityQueue()
for i in range(n):
    for j in range(n):
        q.put((len(board[i][j]), i, j))

if solve():
    print_board(board)
else:
    print("Unsolvable")

print("Solved in", time() - start, "seconds")
